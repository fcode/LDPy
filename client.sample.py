#!/usr/bin/env python3
# coding: utf8

"""Start OpenSearch client.

Filename: client.py

***REMOVED***
"""

from opensearchpy import OpenSearch

LDP_ALIAS = "<xxx>"
LDP_CLUSTER = "gra<xxx>.logs.ovh.com"
LDP_CRED_TYPE = "token"
LDP_CRED_VALUE = "<xxx>"
LDP_STREAM_NAME = "My awesome LDP stream"

opnsrch_clt = OpenSearch(
    LDP_CLUSTER,
    http_auth=(LDP_CRED_VALUE, LDP_CRED_TYPE),
    scheme="https",
    port=9200,
)
